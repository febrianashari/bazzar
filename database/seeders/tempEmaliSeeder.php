<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use DB;

class tempEmaliSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('temp_email')->insert([
            'jns_message' => 'ID',
            'message' => 'Hallo bapak/ibu yang berbahagia dengan ini anda di undang untuk dapat hadir ke acara HUNTBAZZAR yang akan di adakan pada tanggal 24 November 2021 oleh karena itu dimohon untuk bapak ibu mengklik link yang terlampir untuk konfirmasi kehadiran pada bazzar',
            'created_at' => \Carbon\Carbon::now(),
            'upd' => 'admin',
        ]);
        DB::table('temp_email')->insert([
            'jns_message' => 'EN',
            'message' => 'Hallo Mr/Mrs this email to invite you come in HUNTBAZZAR AT blablabla which will be held on 24 Nov 2021 silahkan so please follow link <url> ',
            'created_at' => \Carbon\Carbon::now(),
            'upd' => 'admin',

        ]);
    }
}
