<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('invitation_id')->unique();
            $table->string('nama');
            $table->datetime('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('email')->unique();
            $table->string('designer_favorit');
            $table->string('status');
            $table->integer('is_active');
            $table->string('upd');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
