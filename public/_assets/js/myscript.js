$(function() {
    $("#dasboard a:contains('Halaman Utama')").parent().addClass('active');
    $("#pasien a:contains('Data Pasien')").parent().addClass('active');
    $("#obat a:contains('Data Obat')").parent().addClass('active');
    $("#dokter a:contains('Data Dokter')").parent().addClass('active');
    $("#periksa a:contains('Pemeriksaan')").parent().addClass('active');
    $("#rekam_medis a:contains('Data Rekam Medis')").parent().addClass('active');
    $("#penyakit a:contains('Data Penyakit')").parent().addClass('active');
    $("#user a:contains('Data User')").parent().addClass('active');
    $("#laporan_kunjungan a:contains('Kunjungan')").parent().addClass('active');
    $("#laporan_obat a:contains('Penggunaan Obat')").parent().addClass('active');
    $("#laporan_penyakit a:contains('Besar Penyakit')").parent().addClass('active');
    $("#pesan_individu a:contains('Pesan Individu')").parent().addClass('active');
    $("#pesan_broadcast a:contains('Pesan Broadcast')").parent().addClass('active');
    $("#pesan_masuk a:contains('Pesan Masuk')").parent().addClass('active');
    $("#pesanbanding a:contains('Pesan Efektif')").parent().addClass('active');
    $("#pesan_pengingat a:contains('Pengingat Pasien')").parent().addClass('active');
    $("#akun_pasien a:contains('Akun Pasien')").parent().addClass('active');
    // $("#pesan_efektif a:contains('Pesan Efektif')").parent().addClass('active');
});