<?php

use Illuminate\Support\Facades\Route;
use App\Models\admin\adminModels;
use App\Models\admin\MailModel;
use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});
Route::get('/huntbazzar/{id}', [App\Http\Controllers\mulaiController::class, 'show']);
Route::post('/addCustomer', [App\Http\Controllers\mulaiController::class, 'addCustomer'])->name('addCustomer');


Auth::routes();
Route::prefix('admin')->group(function (){
    Route::get('/home', [App\Http\Controllers\admin\indexController::class, 'index'])->name('home');
    Route::post('/addInvitation', [App\Http\Controllers\admin\indexController::class, 'addInvitation'])->name('addInvitation');
    Route::post('/getInvitation', [App\Http\Controllers\admin\indexController::class, 'getInvitation'])->name('getInvitation');
    Route::post('/deleteInvitation', [App\Http\Controllers\admin\indexController::class, 'deleteInvitation'])->name('deleteInvitation');
    // jtable
    Route::post('/getTableInvitation', function(Request $request) {
        $input = $request->all();
        $jtPageSize = $request->jtPageSize;
        $jtStartIndex = $request->jtStartIndex;
        $invitation_users = adminModels::skip($jtStartIndex)->limit($jtPageSize)->get(); 
        $hasil = adminModels::all()->toArray();
        $count_invitation= count($hasil);
        $result = array(
            'Result' => 'OK',
            'Records' => $invitation_users,
            "TotalRecordCount" => "$count_invitation"

        );
        return $result;
    })->name('getTableInvitation');
    //jtable
    Route::post('/kirim-email',[App\Http\Controllers\MailController::class, 'index'])->name('SendEmail');
    Route::get('/tempEmail', [App\Http\Controllers\admin\indexController::class, 'tempEmail'])->name('tempEmail');
    //jtable
    Route::post('/getTableTempEmail', function(Request $request) {
        $input = $request->all();
        $jtPageSize = $request->jtPageSize;
        $jtStartIndex = $request->jtStartIndex;
        $invitation_users = MailModel::skip($jtStartIndex)->limit($jtPageSize)->get(); 
        $hasil = MailModel::all()->toArray();
        $count_invitation= count($hasil);
        $result = array(
            'Result' => 'OK',
            'Records' => $invitation_users,
            "TotalRecordCount" => "$count_invitation"

        );
        return $result;
    })->name('getTableEmail');
    // jtable
    Route::post('/getTableDesigner', function(Request $request) {
        $hit = Http::get('https://www.huntstreet.com/designer')
        ->body();
        $client= new GuzzleHttp\Client();
        $res= $client->request('GET','https://www.huntstreet.com/designer');
        $result = $res->getBody();
        $tes = html_entity_decode($hit);

        $input = $request->all();
        $jtPageSize = $request->jtPageSize;
        $jtStartIndex = $request->jtStartIndex;
        $invitation_users = MailModel::skip($jtStartIndex)->limit($jtPageSize)->get(); 
        $hasil = MailModel::all()->toArray();
        $count_invitation= count($hasil);
        $result = array(
            'Result' => 'OK',
            'Records' => $invitation_users,
            "TotalRecordCount" => "$count_invitation",
            "resut" => $tes
        );
        return $result;
    })->name('getTableDesigner');
    // jtable

});
