<!DOCTYPE html>
<html lang="en">

<head>
<title>Selamat Datang</title>
@include('layoutsxx.head_user')
</head>
<body>
    <!-- Page Content -->
    <div id= "content">
      <div class="right_col" role="main">
        <div class="row tile_count">
          <?php
            setlocale(LC_ALL, 'IND');
            $day = strftime('%d %B %Y');
            $hari = date('d');
            $bln = date('m');
            $year = date('Y');
          ?>
          <div class="col-md-12 col-sm-4 col-xs-6">
              <h3 class="count_top text-center"><i class="fa fa-user"></i> Data Undangan Huntstreet {{$day}}</h3>
              <button class="btn btn-info" data-toggle="modal" data-target="#modal_add_invitation" type="button"> Add New Invitation</button>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Undangan Laki-Laki</span>
            <div class="count" id="count_pria">0</div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Undangan Perempuan</span>
            <div class="count" id="count_wanita">0</div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Undangan Terkonfirmasi</span>
            <div class="count">0</div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Data Invitation User<small>Huntstreet</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a></li>
                      <li><a href="#">Settings 2</a></li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <br />
              <div class="monitoring">
                <h3 class="mt-4">Data Invitation User</h3>
                  <div id="table_invitation_user"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2>Data Pasien Trimester<small>{{$year}}</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a></li>
                      <li><a href="#">Settings 2</a></li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
              <div id="LineChart"></div>
            </div>
          </div>
        </div>
      </div>

      <div aria-hidden="true" class="modal fade" id="modal_add_invitation" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_add_invitation">
          {{ csrf_field() }}
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Add Invitation</h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
              </div>
              <div class="modal-body row">
                <div class="form-group col-md-4">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="name" required>
                </div>
                <div class="form-group col-md-4">
                    <label>No Hp</label>
                    <input type="text" class="form-control" name="no_hp" required>
                </div>
                <div class="form-group col-md-4">
                    <label>Email</label>
                    <input id="email" type="email" class="form-control" name="email" required>
                </div>
                <div class="form-group col-md-6">
                    <label>Jenis Kelamin</label>
                    <select class="form-control" name="jns_kelamin" required>
                        <option disabled selected value> Pilih Jenis Kelamin</option>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>Jenis Message</label>
                    <select class="form-control" name="jns_message" required>
                        <option disabled selected value> Pilih Bahasa </option>
                        <option value="EN">EN</option>
                        <option value="ID">ID</option>
                    </select>
                </div>
              </div>
              <div class="modal-footer">
                  <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
                  <button class="btn btn-primary" type="submit"> Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
@include('layoutsxx.footer')

    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

    $(document).ready(function(){
      getInvitation();
      $("#data_hamil").DataTable({
        columnDefs: [{
          "orderable":false,
          "targets": [0,1,2,3,4,6,5,7,8,9]
        }],
        "order": [7, "asc"],
      });


      $("#table_invitation_user").jtable({
        paging: true,
        pageSize: 10,
        sorting: true,
        defaultSorting: "id DESC",
        actions: {
            listAction: "{{ route('getTableInvitation') }}"
        },
        fields: {
            nama: {
                title: "Nama",
                width: "3%"
            },
            no_hp: {
                title: "No HP",
                width: "3%"
            },
            jenis_kelamin: {
                title: "Jenis Kelamin",
                width: "7%"
            },
            email: {
                title: "Email",
                width: "3%"
            },
            jns_message: {
                title: "Jenis Message",
                width: "7%"
            },
            url: {
                title: "Link",
                width: "8%"
            },
            is_active: {
                title: "Status Email Send",
                width: "5%",
                cssClass: "textright",
                display: function(data) {
                    if (data.record.is_active == 1) {
                        return "Has Send";
                    } else if (data.record.is_active == 0) {
                        return "Not Send";
                    } 
                }
            },
            button: {
                title: "Action",
                width: "3%",
                cssClass: "textright",
                display: function(data) {
                    return ('<button type="button" data-id="' + data.record.id + '" class="btn btn-black btn-release">Release</button>');
                    // return ('<button type="button" data-id="' + data.record.userid + '" class="btn btn-sm blue btn-manage">Manage</button>');
                }
            }
        }
      });


      $("#table_invitation_user").jtable("load", {
        _token: "{{ csrf_token() }}"
      });
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#form_add_invitation").submit(function(event) {
      email = $(".email").val();
      $.ajax({
          url: "{{ route('addInvitation') }}",
          type: 'POST',
          dataType: "json",
          data: $("#form_add_invitation").serialize(),
          success: function(data) {
              // log response into console
              console.log(data.status);
              if(data.status !=  true){
                notif_error('Duplicate Entry');

              }else{
                sendMessage(data.email);
                getInvitation();
                $("#table_invitation_user").jtable("load", {
                  _token: "{{ csrf_token() }}"
                });
                
                notif_success('Add Invitation Success');
                $("#modal_add_invitation").modal('hide');
              }
          }
      });
      event.preventDefault();
    });
    function getInvitation() {
      _token = $("._token").val();
        $.ajax({
          url: "{{ route('getInvitation') }}",
          type: 'POST',
          dataType: "json",
          data: {
                _token: "{{ csrf_token() }}",
                getInvitation: '1'
            },
          success: function(data) {
              // log response into console
              console.log(data);
						  $("#count_pria").html(data.count_pria);
						  $("#count_wanita").html(data.count_wanita);

          }
      });

    }
    const sendMessage = (email) => {
      
      $.ajax({
        url: `{{ route('SendEmail') }}`,
        cache: false,
        type: "POST",
        data:  {
                _token: "{{ csrf_token() }}",
                email: email
            },
        dataType: "json",
        success: function (result) {
            console.log(result);
        }
      });
    }
    $("#table_invitation_user").on("click", ".btn-release", function(event) {
        let id = $(this).data('id');
        $.ajax({
            url: "{{ route('deleteInvitation') }}",
            type: 'post',
            data: {
                _token: "{{ csrf_token() }}",
                id: id,
            },
            dataType: "json",
            success: function(data) {
              if(data.status !=  true){
                notif_error('Data tidak ditemukan');
              }else{
                getInvitation();
                $("#table_invitation_user").jtable("load", {
                  _token: "{{ csrf_token() }}"
                });
                notif_success('Release User Success');
              }
            },
        });
    });


  </script>
</body>
</html>
