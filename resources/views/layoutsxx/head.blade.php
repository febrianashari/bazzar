<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('../css/jquery.dataTables.css')}}">
    <link href="{{asset('../admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link href="{{asset('../vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('../vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('../vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset('../vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{asset('../vendors/google-code-prettify/bin/prettify.min.css')}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{asset('../vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{asset('../vendors/switchery/dist/switchery.min.css')}}" rel="stylesheet">
    <!-- starrr -->
    <link href="{{asset('../vendors/starrr/dist/starrr.css')}}" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="{{asset('../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{asset('../vendors/jqvmap/dist/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset('../vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('../build/css/custom.css')}}" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container" style="background-color:#2A3F54;">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; background-color:#EDEDED; position:relative;">
              <a href="/manajemen" class="site_title"><img src="{{asset('../admin/image/LogoPuriAdisty.png')}}" alt=""  style="margin-left:12px; margin-bottom:7px; width:200px;"></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_info text-center">
                <span>Welcome,</span>
                <h2>{{ Auth::user()->name }}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="/manajemen"><i class="fa fa-home"></i> Home</span></a></li>
                  <li><a href="/manajemen/datapasien"><i class="fa fa-edit"></i> Data Pasien </a></li>
                  <li><a><i class="fa fa-stethoscope"></i> Periksa Pasien <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/manajemen/pemeriksaan_umum">Umum</a></li>
                      <li><a href="/manajemen/pemeriksaan_gigi">Gigi</a></li>
                      <li><a href="/manajemen/pemeriksaan_kb">KB</a></li>
                      <li><a href="/manajemen/pemeriksaan_kehamilan">Kehamilan</a></li>
                    </ul>
                  </li>
                  <li><a href="/manajemen/data_periksa"><i class="fa fa-stethoscope"></i> Data Periksa </a></li>
                  <li><a href="/manajemen/obat"><i class="fa fa-medkit"></i> Data Obat </a></li>
                  <li><a><i class="fa fa-desktop"></i> Rekam Medis <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/manajemen/rekamMedU">Umum</a></li>
                      <li><a href="/manajemen/rekamMedG">Gigi</a></li>
                      <li><a href="/manajemen/rekamMedK">KIA</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i>Kunjungan Pasien <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/manajemen/detail_umum">Umum</a></li>
                      <li><a href="/manajemen/detail_gigi">Gigi</a></li>
                      <li><a href="/manajemen/detail_kia_verif">KIA</a></li>
                    </ul>
                  </li>
                  <li><a href="/manajemen/pembayaran"><i class="fa fa-money"></i> Pembayaran</a>
                  <li><a href="/manajemen/laporan_keuangan"><i class="fa fa-bar-chart-o"></i> Laporan Keuangan</a>                  
                  </li>
                  <li><a><i class="fa fa-clone"></i>Profil <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/manajemen/dataB">Bidan</a></li>
                      <li><a href="/manajemen/dataD">Dokter</a></li>
                      <li><a href="/manajemen/dataPe">Petugas</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle" style="color:#5A738E">
                <a id="menu_toggle" style="color:#5A738E"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      {{ Auth::user()->name }} <span class="fa fa-chevron-down"></span></a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a  class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" style="font-size:15px;">
                            <i class="fa fa-sign-out pull-right"></i>{{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                        </form>
                    </li>
                    <li>
                      <a href="/manajemen/edit_password" style="font-size:15px;">Ganti Password</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
    <script src="{{asset('../datatable/jquery1/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('../_assets/js/jquery-ui-1.12.1/jquery-ui.js')}}"></script>
    <script src="{{asset('../admin/vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('../admin/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <!-- jQuery -->
    <!-- Bootstrap -->
    <script src="{{asset('../vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('../vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('../vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{asset('../vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- gauge.js -->
    <script src="{{asset('../vendors/gauge.js/dist/gauge.min.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{asset('../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('../vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{asset('../vendors/skycons/skycons.js')}}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{asset('../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
    <script src="{{asset('../vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
    <script src="{{asset('../vendors/google-code-prettify/src/prettify.js')}}"></script>
    <!-- Flot -->
    <script src="{{asset('../vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{asset('../vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('../vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('../vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('../vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{asset('../vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{asset('../vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{asset('../vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{asset('../vendors/jquery.tagsinput/src/jquery.tagsinput.js')}}"></script>
    <!-- Switchery -->
    <script src="{{asset('../vendors/switchery/dist/switchery.min.js')}}"></script>
    <!-- DateJS -->
    <script src="{{asset('../vendors/DateJS/build/date.js')}}"></script>
     <!-- Select2 -->
     <script src="{{asset('../vendors/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- Parsley -->
    <!-- Autosize -->
    <script src="{{asset('../vendors/autosize/dist/autosize.min.js')}}"></script>
    <!-- jQuery autocomplete -->
    <script src="{{asset('../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js')}}"></script>
    <!-- starrr -->
    <script src="{{asset('../vendors/starrr/dist/starrr.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{asset('../vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
    <script src="{{asset('../vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{asset('../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{asset('../vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('../vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{asset('../build/js/custom.min.js')}}"></script>

  <!-- Custom scripts for all pages-->

</body>
</html>