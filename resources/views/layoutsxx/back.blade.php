<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('../css/jquery.dataTables.css')}}">
  <link rel="stylesheet" href="{{asset('../bootstrap/css/style.css')}}">
  <!-- <link href="{{asset('../fonts/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"> -->
  <link href="{{asset('../_assets/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('../_assets/css/now-ui-dashboard.css?v=1.0.1')}}" rel="stylesheet" />
  <link href="{{asset('../_assets/css/select2.min.css')}}" rel="stylesheet" />

  <link href="{{asset('../_assets/js/jquery-ui-1.12.1/jquery-ui.css')}}" rel="stylesheet">
  <link href="{{asset('../admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('../css/simple-sidebar.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('../css/style.css')}}">




  <!-- Custom styles for this template -->
  <div id="page-content-wrapper">
</head>
<body>
      <nav class="nav navbar-expand-lg navbar-dark border-bottom" id="head-content" style="color:white; 	background: linear-gradient(to right,#1dd62d, green); height:100px;">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <div class="sidebar-heading">
        <div class="before-heading text-center" style="background-color:#fff; margin-top:10px; margin-left:10px; width:250px; height:80px; border-radius:20px;">
      <a href="/petugas" class="heading" style="color:#000;"><img src="{{asset('../admin/image/LogoPuriAdisty.png')}}" alt=""  style="width:230px; margin-top:5px;"> </a>
      </div>
      </div>
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            @guest
              <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>
              @if (Route::has('register'))
              <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>
              @endif
              @else
                <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:#fff; border-radius:20px; color:#000;" v-pre>
                      {{ Auth::user()->name }}
                  </a>

                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                 </div>
              </li>
          @endguest
          </ul>
        </div>
      </nav>
      <div class="d-flex" id="wrapper">
      <style>
    body{
      background-color:#dce2dd;
    }
    a.list-group-item{
      background-color:#1dd62d;
    }
    
    </style>
    <!-- Sidebar -->
    <div class="card" style="position:relative; width:250px; box-shadow: 5px 5px 10px; background-color:#1dd62d">
    <div class="border-right" id="sidebar-wrapper">
      <div class="list-group list-group-flush" id="side">
        <a href="/petugas" class="list-group-item list-group-item-action" style= 'color:black;'>Dashboard</a>
        <a href="/petugas/tambahP" class="list-group-item list-group-item-action " style= 'color:black;'>Tambah Pasien</a>
        <a href="/petugas/data" class="list-group-item list-group-item-action " style= "color:black;">Cari Pasien</a>
        <div class="dropdown" >

          <a  href="#" class="list-group-item list-group-item-action dropdown-toggle" data-toggle="dropdown" style= "color:black;">
              Rekam Medis
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/petugas/rekamMedGigi">Gigi</a>
              <a class="dropdown-item" href="/petugas/rekamMedKia">KIA</a>
              <a class="dropdown-item" href="/petugas/rekamMedUmum">Umum</a>
          </div>
        </div>
        <a href="/petugas/pembayaran" class="list-group-item list-group-item-action" style= "color:black;">Pembayaran</a>
        <div class="dropdown" >

          <a  href="#" class="list-group-item list-group-item-action dropdown-toggle" data-toggle="dropdown" style= "color:black;">
              Profile
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/petugas/dataD">Dokter</a>
              <a class="dropdown-item" href="/petugas/dataB">Bidan</a>
              <a class="dropdown-item" href="/petugas/dataPe">Petugas Jaga</a>
          </div>
        </div>
      </div>
    </div>
    </div>

      <script src="{{asset('../fonts/jquery/jquery.min.js')}}"></script>
      <script src="{{asset('../datatable/jquery1/jquery-1.12.4.min.js')}}"></script>
      <script src="{{asset('../js/jquery.dataTables.js')}}"></script>
      <script src="{{asset('../js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('../fonts/bootstrap/js/bootstrap.bundle.min.js')}}"></script>


  <!-- Custom scripts for all pages-->
  <!-- Demo scripts for this page-->
</body>
</html>