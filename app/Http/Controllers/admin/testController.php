<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

class testController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function show()
    {
        return view('/admin/test', ['name' => "ini"]);
    }
}
