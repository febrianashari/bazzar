<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\admin\adminModels;
use App\Models\admin\emailModels;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Auth;
use Carbon\Carbon;


class indexController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function index()
    {
        return view('admin/index');
    }
    public function load_view(Request $request)
    {   
        $view = $request->view;
        return view("admin/$view");
    }
    public function tempEmail()
    {   
        return view('admin/tempEmail');
    }
    public function addInvitation(Request $request)
    {
        $input = $request->all();
        $users = adminModels::where('email', $request->email)->get(); 
        $cek_users = count($users);
        $now = Carbon::now();
        $uniq = uniqid();
        if($cek_users == 1){
            return response()->json([
                "status" => false,
                "data" => "Data Duplicated"
            ]);
        }else{
            $invitation = new adminModels;
            $invitation->nama = $request->name;
            $invitation->no_hp = $request->no_hp;
            $invitation->jenis_kelamin = $request->jns_kelamin ;
            $invitation->email = $request->email;
            $invitation->jns_message = $request->jns_message;
            $invitation->unique_id = $uniq;
            $invitation->url = 'http://localhost:8000/huntbazzar/'.$uniq;
            $invitation->is_active = "0";
            $invitation->status = "0";
            $invitation->upd = Auth::user()->name;
            $invitation->save();

            $customer = new Customer;
            $customer->email = $request->email;
            $customer->is_active = '0';
            $customer->status = '0';
            $customer->upd = Auth::user()->name;
            $customer->created_at = $now;
            $customer->save();
            return response()->json([
                "status" => true,
                "data" => $input,
                "email" => $request->email

            ]);    
        }
    }
    public function getInvitation(Request $request)
    {
        $input = $request->all();
        $users_pria = adminModels::where('jenis_kelamin', 'L')->get(); 
        $count_pria = count($users_pria);
        $users_wanita = adminModels::where('jenis_kelamin', 'P')->get(); 
        $count_wanita = count($users_wanita);
        $data_invitation['count_pria'] = $count_pria;
        $data_invitation['count_wanita'] = $count_wanita;
        return response()->json([
            "status" => true,
            "data" => $data_invitation,
            "count_pria" => $count_pria,
            "count_wanita" => $count_wanita,
        ]);    
    }
    public function deleteInvitation(Request $request)
    {
        $input = $request->id;
        $users = adminModels::where('id', $input)->get(); 
        $cek_users = count($users);
        if($cek_users == 0){
            return response()->json([
                "status" => false,
                "data" => "Data Tidak Ditemukan"
            ]);
        }else{
            adminModels::where('id', $input)->delete();
            return response()->json([
                "status" => true,
                "data" => "Data Berhasil Dihapus"
            ]);    
        }
    }
}
