<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Requests\customerRequest;
use Carbon\Carbon;
use App\Models\admin\adminModels;



class mulaiController extends Controller
{
    //
    public function show($id)
    {
        $users = adminModels::where('unique_id', $id)->first();
        $name = $users->nama;
        $email = $users->email;
        $status = $users->status;
        if($status == 0){
            return view('/landPage', ['name' => $name,'email' => $email]);
        }else{ 
            $email = Customer::where('email', $email)->first();
            $cust_id = $email->invitation_id;
            $update_at = $email->update_at;
            return view('/countDown', ['name' => $name,'cust_id' => $cust_id,'update_at' => $update_at ]);
        }
    }
    public function addCustomer(Request $request)
    {
        $cust_id = 'HB'.uniqid();
        date_default_timezone_set('Asia/Jakarta');
        $now = Carbon::now();
        $email = Customer::where('email', $request->email)->first();
        $status = $email->status;
        if($status == 0){
            $update = Customer::find($email->id);
            $update->invitation_id = $cust_id;
            $update->nama = $request->nama;
            $update->tanggal_lahir = $request->tanggal_lahir;
            $update->jenis_kelamin = $request->jenis_kelamin;
            $update->designer_favorit = json_encode($request->designer_favorite);
            $update->status = '1';
            $update->is_active = '1';
            $update->updated_at = $now;
            $update->save();
    
    
            $users = adminModels::where('email', $request->email)->first();
            $update_admin = adminModels::find($users->id);
            $update_admin->status = '1';
            $update_admin->save();    
        }else{
            $cust_id = $email->invitation_id;
        }


        $name = $request->nama;

        return view('/countDown', ['name' => $name,'cust_id' => $cust_id, 'update_at' => $now]);
    }
}
