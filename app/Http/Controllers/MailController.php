<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\emailSender;
use Illuminate\Support\Facades\Mail;
use App\Models\admin\adminModels;
use App\Models\admin\MailModel;

class MailController extends Controller
{
    //
    public function index(Request $request){

        
        $users = adminModels::where('email', $request->email)->first();
        $temp = MailModel::where('jns_message', $users->jns_message)->first();
        $url = $users->url;
        $message = str_replace('<url>', $url, $temp->message);
        
        $details = [
        'title' => 'Invitation HuntBazzars',
        'body' => "$message"
        ];       
        \Mail::to($request->email)->send(new \App\Mail\emailSender($details));
        $update = adminModels::find($users->id);
        $update->is_active = '1';
        $update->save();
 
        return response()->json([
            "status" => true,
            "data" => $users->id,
            "result" => "Email Terkirim"

        ]);  
    }
}
