<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class adminModels extends Model
{
    use HasFactory;
    protected $table = 'invitation_users';
    protected $primaryKey = 'id';
}
