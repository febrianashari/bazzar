<!DOCTYPE html>
<html lang="en">

<head>
<title>Temp Email</title>
<?php echo $__env->make('layoutsxx.head_user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</head>
<body>
    <!-- Page Content -->
    <div class="right_col" role="main">
    <?php
          setlocale(LC_ALL, 'IND');
          $day = strftime('%d %B %Y');
          $hari = date('d');
          $bln = date('m');
          $year = date('Y');
        ?>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h3>Template Email<small> Huntstreet</small></h3>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a></li>
                    <li><a href="#">Settings 2</a></li>
                  </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
              </ul>
          </div>
          <div class="x_content">
            <div class="monitoring">
              <h3 class="mt-4">Template Email</h3>
                <div id="table_temp_email"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php echo $__env->make('layoutsxx.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <!-- /#page-content-wrapper -->
  <!-- /#wrapper -->
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

    $(document).ready(function(){
      getInvitation();
      $("#data_hamil").DataTable({
        columnDefs: [{
          "orderable":false,
          "targets": [0,1,2,3,4,6,5,7,8,9]
        }],
        "order": [7, "asc"],
      });


      $("#table_temp_email").jtable({
        paging: true,
        pageSize: 10,
        sorting: true,
        defaultSorting: "id DESC",
        actions: {
            listAction: "<?php echo e(route('getTableEmail')); ?>"
        },
        fields: {
            jns_message: {
                title: "Jenis Message",
                width: "4%"
            },
            message: {
                title: "No HP",
            },
            button: {
                title: "Action",
                width: "3%",
                cssClass: "textright",
                display: function(data) {
                    return ('<button type="button" data-id="' + data.record.id + '" class="btn btn-black btn-update">Update</button>');
                    // return ('<button type="button" data-id="' + data.record.userid + '" class="btn btn-sm blue btn-manage">Manage</button>');
                }
            }
        }
    });


      $("#table_temp_email").jtable("load", {
        _token: "<?php echo e(csrf_token()); ?>"
      });
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#form_add_invitation").submit(function(event) {
      email = $(".email").val();
      $.ajax({
          url: "<?php echo e(route('addInvitation')); ?>",
          type: 'POST',
          dataType: "json",
          data: $("#form_add_invitation").serialize(),
          success: function(data) {
              // log response into console
              console.log(data.status);
              if(data.status !=  true){
                notif_error('Duplicate Entry');

              }else{
                sendMessage(data.email);
                getInvitation();
                $("#table_invitation_user").jtable("load", {
                  _token: "<?php echo e(csrf_token()); ?>"
                });
                
                notif_success('Add Invitation Success');
                $("#modal_add_invitation").modal('hide');
              }
          }
      });
      event.preventDefault();
    });
    function getInvitation() {
      _token = $("._token").val();
        $.ajax({
          url: "<?php echo e(route('getInvitation')); ?>",
          type: 'POST',
          dataType: "json",
          data: {
                _token: "<?php echo e(csrf_token()); ?>",
                getInvitation: '1'
            },
          success: function(data) {
              // log response into console
              console.log(data);
						  $("#count_pria").html(data.count_pria);
						  $("#count_wanita").html(data.count_wanita);

          }
      });

    }
    const sendMessage = (email) => {
      
      $.ajax({
        url: `<?php echo e(route('SendEmail')); ?>`,
        cache: false,
        type: "POST",
        data:  {
                _token: "<?php echo e(csrf_token()); ?>",
                email: email
            },
        dataType: "json",
        success: function (result) {
            console.log(result);
            loading_end();
        }
      });
    }


  </script>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\belajar_laravel\resources\views/admin/tempEmail.blade.php ENDPATH**/ ?>