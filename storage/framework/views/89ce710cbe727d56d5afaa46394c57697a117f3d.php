<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('../css/jquery.dataTables.css')); ?>">
    <link href="<?php echo e(asset('../admin/vendor/fontawesome-free/css/all.min.css')); ?>" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link href="<?php echo e(asset('../vendors/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo e(asset('../vendors/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo e(asset('../vendors/nprogress/nprogress.css')); ?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo e(asset('../vendors/iCheck/skins/flat/green.css')); ?>" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="<?php echo e(asset('../vendors/google-code-prettify/bin/prettify.min.css')); ?>" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?php echo e(asset('../vendors/select2/dist/css/select2.min.css')); ?>" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?php echo e(asset('../vendors/switchery/dist/switchery.min.css')); ?>" rel="stylesheet">
    <!-- starrr -->
    <link href="<?php echo e(asset('../vendors/starrr/dist/starrr.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('../iziToast/css/iziToast.min.css')); ?>" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="<?php echo e(asset('../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')); ?>" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo e(asset('../vendors/jqvmap/dist/jqvmap.min.css')); ?>" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo e(asset('../vendors/bootstrap-daterangepicker/daterangepicker.css')); ?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo e(asset('../build/css/custom.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('../jtable/themes/metro/lightgray/jtable.css')); ?>" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container" style="background-color:#2A3F54;">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; background-color:#EDEDED; position:relative;">
              <a href="<?php echo e(route('home')); ?>" class="site_title">Hunstreet</a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_info text-center">
                <span>Welcome,</span>
                <h2><?php echo e(Auth::user()->name); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo e(route('home')); ?>"><i class="fa fa-home"></i> Home</span></a></li>
                  <li><a href="<?php echo e(route('tempEmail')); ?>"><i class="fa fa-edit"></i> Template Email </a></li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle" style="color:#5A738E">
                <a id="menu_toggle" style="color:#5A738E"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      <?php echo e(Auth::user()->name); ?> <span class="fa fa-chevron-down"></span></a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a  class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" style="font-size:15px;">
                            <i class="fa fa-sign-out pull-right"></i><?php echo e(__('Logout')); ?>

                        </a>
                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                        <?php echo csrf_field(); ?>
                        </form>
                    </li>
                    <li>
                      <a href="/petugas/edit_password" style="font-size:15px;">Ganti Password</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
    <script src="<?php echo e(asset('../datatable/jquery1/jquery-1.12.4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('../_assets/js/jquery-ui-1.12.1/jquery-ui.js')); ?>"></script>
    <script src="<?php echo e(asset('../admin/vendor/datatables/jquery.dataTables.js')); ?>"></script>
    <script src="<?php echo e(asset('../admin/vendor/datatables/dataTables.bootstrap4.js')); ?>"></script>
    <!-- jQuery -->
    <!-- Bootstrap -->
    <script src="<?php echo e(asset('../vendors/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo e(asset('../vendors/fastclick/lib/fastclick.js')); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo e(asset('../vendors/nprogress/nprogress.js')); ?>"></script>
    <!-- Chart.js -->
    <!-- gauge.js -->
    <script src="<?php echo e(asset('../vendors/gauge.js/dist/gauge.min.js')); ?>"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo e(asset('../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')); ?>"></script>
    <!-- iCheck -->
    <script src="<?php echo e(asset('../vendors/iCheck/icheck.min.js')); ?>"></script>
    <!-- Skycons -->
    <script src="<?php echo e(asset('../vendors/skycons/skycons.js')); ?>"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<?php echo e(asset('../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/jquery.hotkeys/jquery.hotkeys.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/google-code-prettify/src/prettify.js')); ?>"></script>
    <!-- Flot -->
    <script src="<?php echo e(asset('../vendors/Flot/jquery.flot.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/Flot/jquery.flot.pie.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/Flot/jquery.flot.time.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/Flot/jquery.flot.stack.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/Flot/jquery.flot.resize.js')); ?>"></script>
    <!-- Flot plugins -->
    <script src="<?php echo e(asset('../vendors/flot.orderbars/js/jquery.flot.orderBars.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/flot-spline/js/jquery.flot.spline.min.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/flot.curvedlines/curvedLines.js')); ?>"></script>
    <!-- jQuery Tags Input -->
    <script src="<?php echo e(asset('../vendors/jquery.tagsinput/src/jquery.tagsinput.js')); ?>"></script>
    <!-- Switchery -->
    <script src="<?php echo e(asset('../vendors/switchery/dist/switchery.min.js')); ?>"></script>
    <!-- DateJS -->
    <script src="<?php echo e(asset('../vendors/DateJS/build/date.js')); ?>"></script>
     <!-- Select2 -->
     <script src="<?php echo e(asset('../vendors/select2/dist/js/select2.full.min.js')); ?>"></script>
    <!-- Parsley -->
    <!-- Autosize -->
    <script src="<?php echo e(asset('../vendors/autosize/dist/autosize.min.js')); ?>"></script>
    <!-- jQuery autocomplete -->
    <script src="<?php echo e(asset('../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js')); ?>"></script>
    <!-- starrr -->
    <script src="<?php echo e(asset('../vendors/starrr/dist/starrr.js')); ?>"></script>
    <!-- JQVMap -->
    <script src="<?php echo e(asset('../vendors/jqvmap/dist/jquery.vmap.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/jqvmap/dist/maps/jquery.vmap.world.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')); ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo e(asset('../vendors/moment/min/moment.min.js')); ?>"></script>
    <script src="<?php echo e(asset('../vendors/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo e(asset('../build/js/custom.js')); ?>"></script>
    <script src="<?php echo e(asset('../iziToast/js/iziToast.min.js')); ?>"></script>
    <script src="<?php echo e(asset('../jtable/js/jquery-ui-1.11.2.min.js')); ?>"></script>
    <script src="<?php echo e(asset('../jtable/js/jquery.jtable-2.4.0.min.js')); ?>"></script>
    <script src="<?php echo e(asset('../jtable/jquery.jtable.js')); ?>"></script>

    <script>

      iziToast.settings({
        timeout: 4000,
        position: "bottomRight",
        close: true,
        transitionIn: "bounceInLeft",
        transitionOut: "fadeOutRight"
      });
      const notif_success = (message) => {
        iziToast.success({
            title: "Good Job",
            message: message
        });
      }
      const notif_warning = (message) => {
          iziToast.warning({
              title: "Warning",
              message: message
          });
      }

      const notif_error = (message) => {
          iziToast.error({
              title: "Error",
              message: message
          });
      }

    </script>

  <!-- Custom scripts for all pages-->

</body>
</html><?php /**PATH C:\xampp\htdocs\belajar_laravel\resources\views/layoutsxx/head_user.blade.php ENDPATH**/ ?>